import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'

// https://vitejs.dev/config/
export default defineConfig((command, mode) => {
	if (command === 'build') {
		return {
			plugins: [svelte()],
		}
	} else {
		return {
			server: {
				proxy: {
					'/api': {
						/* Default API port is 8080 */
						target: "http://localhost:8080/",
						changeOrigin: true,
						rewrite: (path) => path.replace(/^\/api/, '')
					}
				},
				cors: true,
			},
			plugins: [svelte()],
		}
	}
})
