// Show user their name on login
(async function() {
	const teacherDetails = await fetch("/api/teacher/account/details").then((res) => res.json());
	console.log(teacherDetails);
	alert(`Welcome to Galvan, ${teacherDetails.profile.first_name}!`);
})()


document.getElementById("add-student-button").addEventListener("click", addStudent);
document.querySelectorAll("#student-list > li").forEach((item) => item.addEventListener("click", openStudentDetailsWindow));

document.getElementById("button-add-event").addEventListener("click", addClassEvent);
document.querySelectorAll(".events-list li").forEach((item) => item.addEventListener("click", openClassEventDetailsWindow));



//
// Open Student Event Details window
//
function openAssignedEventDetailsWindow(event) {
	let eventDetailsWindow = document.createElement("div");
	eventDetailsWindow.classList.add("dialog-window");
	eventDetailsWindow.classList.add("nested-window");
	eventDetailsWindow.setAttribute("id", "event-details");
	eventDetailsWindow.innerHTML = assignedEventDetailsWindowHTML();

	document.body.append(eventDetailsWindow);
	document.getElementById("main-page").classList.add("dimmer");
	eventDetailsWindow.previousElementSibling.classList.add("dim");

	document.querySelector("#event-details button.close-window").addEventListener("click", (event) => {
		eventDetailsWindow.previousElementSibling.classList.remove("dim");
		document.getElementById("main-page").classList.remove("dimmer");
		eventDetailsWindow.remove();
	});
}


//
// Open Class Event Details window
//
function openClassEventDetailsWindow(event) {
	let eventDetailsWindow = document.createElement("div");
	eventDetailsWindow.classList.add("dialog-window");
	eventDetailsWindow.setAttribute("id", "event-details");
	eventDetailsWindow.innerHTML = classEventDetailsWindowHTML();

	document.body.append(eventDetailsWindow);
	document.getElementById("main-page").classList.add("dim");

	document.querySelector("#event-details button.close-window").addEventListener("click", (event) => {
		eventDetailsWindow.remove();
		document.getElementById("main-page").classList.remove("dim");
	});
}

//
// Add Class Event
//
function addClassEvent(event) {
	let addEventWindow = document.createElement("div");
	addEventWindow.setAttribute("id", "add-event");
	addEventWindow.classList.add("dialog-window");
	addEventWindow.innerHTML = addClassEventHTML();

	document.getElementById("main-page").classList.add("dim");
	document.body.append(addEventWindow);

	document.getElementById("button-save-event").addEventListener("click", (event) => {
		event.preventDefault();
		let eventDetails = new FormData(event.target.parentElement);
		let eventElement = document.createElement("li");
		eventElement.classList.add("public-event");
		eventElement.innerText = eventDetails.get("title");
		eventElement.addEventListener("click", openClassEventDetailsWindow);

		let day = eventDetails.get("day");
		document.querySelector(`#days-container #${day} .events-list`).append(eventElement);

		addEventWindow.remove();
		document.getElementById("main-page").classList.remove("dim");
	});


	document.getElementById("button-close-window").addEventListener("click", (event) => {
		addEventWindow.remove();
		document.getElementById("main-page").classList.remove("dim");
	});
}

//
// Add Assigned Event
//
function addAssignedEvent(event) {
	let addEventWindow = document.createElement("div");
	addEventWindow.setAttribute("id", "add-event");
	addEventWindow.classList.add("dialog-window");
	addEventWindow.classList.add("nested-window");
	addEventWindow.innerHTML = addAssignedEventHTML();

	document.body.append(addEventWindow);
	addEventWindow.previousElementSibling.classList.add("dim");
	document.getElementById("main-page").classList.add("dimmer");

	document.getElementById("button-save-event").addEventListener("click", (event) => {
		event.preventDefault();
		let eventDetails = new FormData(event.target.parentElement);
		let eventElement = document.createElement("li");
		eventElement.classList.add("teacher-event");
		eventElement.innerText = eventDetails.get("title");
		eventElement.addEventListener("click", openAssignedEventDetailsWindow);

		let day = eventDetails.get("day");
		document.querySelector(`#student-details #${day} .events-list`).append(eventElement);

		addEventWindow.previousElementSibling.classList.remove("dim");
		document.getElementById("main-page").classList.remove("dimmer");
		addEventWindow.remove();
	});


	addEventWindow.querySelector(".close-window").addEventListener("click", (event) => {
		addEventWindow.previousElementSibling.classList.remove("dim");
		document.getElementById("main-page").classList.remove("dimmer");
		addEventWindow.remove();
	});
}



//
// Add Student to Class
//
function addStudent(event) {
	let addStudentWindow = document.createElement("div");
	addStudentWindow.setAttribute("id", "add-student");
	addStudentWindow.classList.add("dialog-window");
	addStudentWindow.innerHTML = addStudentWindowHTML();

	document.getElementById("main-page").classList.add("dim");
	document.body.append(addStudentWindow);

	document.getElementById("button-close-window").addEventListener("click", (event) => {
		addStudentWindow.remove();
		document.getElementById("main-page").classList.remove("dim");
	});

	document.getElementById("button-save-student").addEventListener("click", (event) => {
		event.preventDefault();

		let studentDetails = new FormData(event.target.parentElement);

		let studentElement = document.createElement("li");
		studentElement.addEventListener("click", openStudentDetailsWindow);
		studentElement.innerText = studentDetails.get("first-name") + " " + studentDetails.get("last-name");

		document.getElementById("student-list").append(studentElement);

		addStudentWindow.remove();
		document.getElementById("main-page").classList.remove("dim");
	});
}



//
// Show Student Details window
//
function openStudentDetailsWindow(event) {
	let studentDetailsWindow = document.createElement("div");
	studentDetailsWindow.setAttribute("id", "student-details");
	studentDetailsWindow.classList.add("dialog-window")
	studentDetailsWindow.innerHTML = studentDetailsWindowHTML();

	document.getElementById("main-page").classList.add("dim");
	document.body.append(studentDetailsWindow);

	document.getElementById("button-close-window").addEventListener("click", (event) => {
		studentDetailsWindow.remove();
		document.getElementById("main-page").classList.remove("dim");
	});

	studentDetailsWindow.querySelector("#add-event-button").addEventListener("click", addAssignedEvent);
	studentDetailsWindow.querySelectorAll(".events-list li").forEach((item) => item.addEventListener("click", openAssignedEventDetailsWindow));
}


const studentDetailsWindowHTML = () => {
	return `
<header>
	<h2>John Doe</h2>
	<button>Edit</button>
	<button id="button-close-window">X</button>
</header>
<section id="student-events-container">
<!-- STUDENT WEEK -->
	<header class="section-header">
		<h3>Student Events</h3>
		<button id="add-event-button">+ Add Event</button>
	</header>
	<div id="days-container">
		<div class="day" id="monday">
			<h4 class="day-name">Mon</h4 class="day-name">
			<ul class="events-list">
			</ul>
			<button class="arrow-button">&rightarrow;</button>
		</div>
		<div class="day" id="tuesday">
			<h4 class="day-name">Tues</h4 class="day-name">
			<ul class="events-list">
			</ul>
			<button class="arrow-button">&rightarrow;</button>
		</div>
		<div class="day" id="wednesday">
			<h4 class="day-name">Wed</h4 class="day-name">
			<ul class="events-list">
				<li class="public-event">Library Field Trip</li>
			</ul>
			<button class="arrow-button">&rightarrow;</button>
		</div>
		<div class="day" id="thursday">
			<h4 class="day-name">Thurs</h4 class="day-name">
			<ul class="events-list">
			</ul>
			<button class="arrow-button">&rightarrow;</button>
		</div>
		<div class="day" id="friday">
			<h4 class="day-name">Fri</h4 class="day-name">
			<ul class="events-list">
			</ul>
			<button class="arrow-button">&rightarrow;</button>
		</div>
	</div>
</section>
<section id="student-reflections">
	<header class="section-header">
		<h3>Reflections</h3>
	</header>
	<div id="reflections-grid">
	</div>
</section>
`
};

const classEventDetailsWindowHTML = () => {
	return `
<header id="event-details-header">
	<button class="close-window">X</button>
	<h2>Event details</h2>
</header>
<section>
	<h3 id="event-title">Library Field Trip</h3>
	<p id="event-description">Class field trip to the library. Don't forget your library card!</p>
</section>
`
}

const assignedEventDetailsWindowHTML = () => {
	return `
<header id="event-details-header">
	<button class="close-window">X</button>
	<h2>Event details</h2>
</header>
<section>
	<h3 id="event-title">Library Field Trip</h3>
	<p id="event-description">Class field trip to the library. Don't forget your library card!</p>
</section>
`
};

const addStudentWindowHTML = () => {
	return `
<header id="add-student-header">
	<button id="button-close-window">X</button>
	<h2>Add Student</h2>
</header>
<form class="creation-form">
	<label for="first-name">First name</label>
	<input type="text" id="input-first-name" name="first-name">
	<label for="last-name">Last name</label>
	<input type="text" id="input-last-name" name="last-name">
	<input type="submit" id="button-save-student" value="Save"></input>
</form>
`
};

const addClassEventHTML = () => {
	return `
<header id="add-event-header">
	<button id="button-close-window">X</button>
	<h2>Add Event</h2>
</header>
<form class="creation-form">
	<label for="title">Title</label>
	<input type="text" id="input-title" name="title">
	<label for="day">Day</label>
	<select id="select-day" name="day">
		<option value="" disabled selected>Select day</option>
		<option value="monday">Monday</option>
		<option value="tuesday">Tuesday</option>
		<option value="wednesday">Wednesday</option>
		<option value="thursday">Thursday</option>
		<option value="friday">Friday</option>
	</select>	
	<label for="description">Description</label>
	<textarea id="input-description" name="description"></textarea>
	<input type="submit" id="button-save-event" value="Save"></input>
</form>
`
};

const addAssignedEventHTML = () => {
	return `
<header id="add-event-header">
	<button class="close-window">X</button>
	<h2>Add Event</h2>
</header>
<form class="creation-form">
	<label for="title">Title</label>
	<input type="text" id="input-title" name="title">
	<label for="day">Day</label>
	<select id="select-day" name="day">
		<option value="" disabled selected>Select day</option>
		<option value="monday">Monday</option>
		<option value="tuesday">Tuesday</option>
		<option value="wednesday">Wednesday</option>
		<option value="thursday">Thursday</option>
		<option value="friday">Friday</option>
	</select>	
	<label for="description">Description</label>
	<textarea id="input-description" name="description"></textarea>
	<input type="submit" id="button-save-event" value="Save"></input>
</form>
`
};
