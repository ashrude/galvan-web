import {writable} from "svelte/store"

export const Students = writable([
	{
		student_id: 13,
		class_id: 20,
		profile: {
			first_name: "John",
			last_name: "Cena"
		}
	},
	{
		student_id: 14,
		class_id: 20,
		profile: {
			first_name: "Johnathan",
			last_name: "Davis"
		}
	},
	{
		student_id: 15,
		class_id: 20,
		profile: {
			first_name: "Doug",
			last_name: "Dougson"
		}
	}
])
