import { writable } from "svelte/store"

export const SidePaneIsRetracted = writable(false)

export const OpenSidePanes = writable([])
