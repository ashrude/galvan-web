import {writable, get} from "svelte/store"

export const Classes = writable([
		{
			class_id: 20,
			class_name: "My Class"
		},
		{
			class_id: 21,
			class_name: "My Better Class"
		},
		{
			class_id: 22,
			class_name: "My Best Class"
		}
])
