import {writable} from "svelte/store"

export const Events = writable([
	{
		event_id: 122,
		event_type: "class",
		event_details: {
			title: "My Tuesday Event",
			date: "2023-01-13",
			weekday: "tuesday"
		}
	},
	{
		event_id: 123,
		event_type: "class",
		event_details: {
			title: "My Event",
			date: "2023-01-12",
			weekday: "monday"
		}
	},
	{
		event_id: 124,
		event_type: "class",
		event_details: {
			title: "My Other Event",
			date: "2023-01-12",
			weekday: "monday"
		}
	}
])
