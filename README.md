# Galvan Web

The web frontend for Galvan.

To get started, see the [API README](https://gitlab.com/galvan1/galvan-api/-/blob/main/README.md).
